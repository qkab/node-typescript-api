import { getModelForClass } from "@typegoose/typegoose";

import { Todo } from "./todos.model";
import { User } from "./users.model";

export const TodoModel = getModelForClass(Todo)
export const UserModel = getModelForClass(User)
