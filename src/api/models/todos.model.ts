import { modelOptions } from '@typegoose/typegoose/lib/modelOptions'
import { prop } from '@typegoose/typegoose/lib/prop'
import { Ref } from '@typegoose/typegoose/lib/types'

import { User } from './users.model'

@modelOptions({
  schemaOptions: {
    toJSON: { virtuals: true },
    toObject: { virtuals: true }
  }
})
export class Todo {
  @prop({ required: true })
  public title!: string

  @prop({ required: true })
  public slug!: string

  @prop({ required: true })
  public description!: string

  @prop({ default: false })
  public completed: boolean

  @prop({ ref: () => User })
  public user: Ref<User>
}
