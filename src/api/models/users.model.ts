import { modelOptions } from '@typegoose/typegoose/lib/modelOptions'
import { pre } from '@typegoose/typegoose/lib/hooks'
import { prop } from '@typegoose/typegoose/lib/prop'
import { Ref } from '@typegoose/typegoose/lib/types'
import { genSalt, hash } from 'bcrypt'

import { Todo } from './todos.model'

@pre<User>('save', async function(){
  const salt =  await genSalt(parseInt(process.env.SALT))
  const hashedPassword = await hash(this.password, salt)

  this.password = hashedPassword
  console.log(this)
})

@modelOptions({
  schemaOptions: {
    toJSON: { virtuals: true },
    toObject: { virtuals: true }
  }
})

export class User {
  @prop({ required: true })
  public firstname: string

  @prop({ required: true })
  public lastname: string

  @prop({ required: true, unique: true })
  public email: string

  @prop({ required: true })
  public password

  @prop()
  public avatar?: string

  @prop({ ref: () => Todo })
  public todos: Ref<Todo>[]

}
