import { Router } from 'express'
import { IExtendedUserRequest } from '../../interfaces/authentication.interface'
import { TodoModel } from '../models/'

const router = Router()

const getSlug = (title: string) => title.trim().toLowerCase().split(' ').join('-')
const userOption = { firstname: 1, lastname: 1 }

router.get('/', async (req: IExtendedUserRequest, res) => {  
  const todos = await TodoModel.find({ user: req.user.id }).lean().populate('user', userOption)
  res.status(200).json(todos)
})

router.post('/', async (req: IExtendedUserRequest, res) => {
  const slug = getSlug(req.body.title)

  const todo = new TodoModel({ ...req.body, slug, user: req.user.id })

  await todo.save()

  return res.status(204)
})

router.get('/:id', async (req: IExtendedUserRequest, res) => {
  const { id } = req.params
  const todo = await TodoModel.findOne({ _id: id, user: req.user.id  }).lean().populate('user', userOption)

  res.status(200).json(todo)
})

router.patch('/:id', async (req: IExtendedUserRequest, res) => {
  const { id } = req.params
  const todo = await TodoModel.findOne({ _id: id, user: req.user.id  })

  if(!todo) {
    return res.status(404).json({ message: "Todo doesn't exist."})
  }

  if(req.body.title) {
    todo.title = req.body.title
    todo.slug = getSlug(req.body.title)
  }

  if(req.body.description) {
    todo.description = req.body.description
  }

  if(req.body.completed) {
    todo.completed = !!req.body.completed
  }

  await todo.save()

  res.status(200).json(todo)
})

router.delete('/:id', async (req: IExtendedUserRequest, res) => {
  const { id } = req.params

  const todo = await TodoModel.findOne({ _id: id, user: req.user.id  })

  if(!todo) {
    return res.status(404).json({ message: "Todo doesn't exist."})
  }

  await todo.delete()

  res.status(200).json({ success: true })
})

export default router