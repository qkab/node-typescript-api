import { compare } from "bcrypt";
import { Router } from "express";
import { sign } from "jsonwebtoken";
import { IUserJWTPayload } from "../../interfaces/authentication.interface";
import { UserModel } from "../models";

const router = Router()

router.post('/register', async (req, res) => {
  try {
    if(req.body.password !== req.body.confirmPassword) {
      return res.status(400).json({ message: "Passwords doesn't match" })
    }
  
    const user = new UserModel({ ...req.body })
  
    await user.save()
  
    res.status(204).json({ success: true })
  } catch (error) {
    if(error.code === 11000) {
      return res.status(500).json({ message: "Email already used" })
    }
  }
})

router.post('/login', async (req, res) => {
  const user = await UserModel.findOne({ email: req.body.email })

  if(!user) {
    return res.status(404).json({ message: "User not found" })
  }

  const isSamePassword = await compare(req.body.password, user.password)

  if(!isSamePassword) {
    return res.status(400).json({ message: "The password doesn't match" })
  }

  const payload: IUserJWTPayload = {
    id: user._id,
    firstname: user.firstname,
    lastname: user.lastname,
    email: user.email,
  }

  const token = sign(payload, process.env.JWT_SECRET, { expiresIn: '2h' })

  res.status(200).json({ token })
})

export default router