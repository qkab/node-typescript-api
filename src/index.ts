import express from 'express'
import helmet from 'helmet'
import { config } from 'dotenv'

import TodosRouter from './api/routes/todos.route'
import UsersRouter from './api/routes/users.route'

import { dbConnect } from './services/mongo.service'
import auth from './middlewares/auth'

config()

const app = express()
const port = process.env.PORT || 5000

app.use(helmet())
app.use(express.json())
app.use(async (_, res, next) => {
  try {
    await next()
  } catch (error) {
    console.error(error)
  }
})

dbConnect()

app.get('/health', (_, res) => {
  res.status(200).json({ success: true })
})

app.use('/api/v1/todos', auth, TodosRouter)
app.use('/api/v1/users', UsersRouter)


app.listen(port, () => console.log(`Server listening on port: ${port}`))