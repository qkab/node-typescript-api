import { Request } from "express";
import { JwtPayload } from "jsonwebtoken";
import { ObjectId } from "mongoose";

export interface IUserJWTPayload {
  id: ObjectId,
  firstname: string,
  lastname: string,
  email: string,
}

export interface IExtendedUserRequest extends Request {
  user: IUserJWTPayload
}