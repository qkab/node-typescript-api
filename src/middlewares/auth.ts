import { NextFunction, Request, Response } from "express";
import { verify } from "jsonwebtoken";
import { IExtendedUserRequest, IUserJWTPayload } from "../interfaces/authentication.interface";


const auth = (req: IExtendedUserRequest, res: Response, next: NextFunction) => {
  try {
    const [, token] = req.headers.authorization.split(' ')
    const payload = verify(token, process.env.JWT_SECRET) as IUserJWTPayload

    req.user = payload

    next()
  } catch (error) {
    console.log(error.message)

    res.status(400).json({ message: "Token not valid" })
  }
}

export default auth