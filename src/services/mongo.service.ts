import { connect } from "mongoose"


export const dbConnect = async (): Promise<void> => {
  try {
    const DB_URL = process.env.MONGODB_URL
    await connect(DB_URL)
    console.log(`DB connectée !`)

  } catch (error) {
    console.error(error)    
  }
}